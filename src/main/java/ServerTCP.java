
/**
 * Created by billobob on 5/16/16.
 */
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class ServerTCP implements Runnable {

    private int          port   = 30000;
    private ServerSocket sock = null;
    private boolean stopped = false;
    private final ExchangeServer exchangeServer;

    public ServerTCP(int port, ExchangeServer ex){
        this.port = port;
        this.exchangeServer = ex;
    }

    public void run(){
        openServerSocket();
        while(!stopped()){
            Socket clientSock = null;
            try {
                clientSock = this.sock.accept();
            } catch (IOException e) {
                if(stopped()) {
                    System.out.println("Server Stopped.") ;
                    return;
                }
                throw new RuntimeException(
                        "Error accepting client connection", e);
            }
            RunLock rl = new RunLock();
            ServerRunnable sv = new ServerRunnable(clientSock, exchangeServer, rl);
            new Thread(sv).start();
        }
        System.out.println("Server Stopped.") ;
    }


    private boolean stopped() {
        return this.stopped;
    }

    public void stop(){
        try {
            this.sock.close();
            this.stopped = true;
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.sock = new ServerSocket(this.port);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Cannot open port %d", this.port), e);
        }
    }

    public int getPort() {
        return port;
    }
}