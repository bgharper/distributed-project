import java.io.Serializable;

/**
 * Created by billobob on 5/24/16.
 */

class RequestVote implements Serializable {
    final int term;
    final String candidateID;
    final int lastLogIndex;
    final int lastLogTerm;
    public RequestVote(int term, String addr, int lli, int llt) {
        this.term = term;
        this.candidateID = addr;
        this.lastLogIndex = lli;
        this.lastLogTerm = llt;
    }
}