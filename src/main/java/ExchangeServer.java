import org.jgroups.*;
import org.jgroups.util.Util;
import org.mapdb.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/* DONE: Implement a true log for consensus
DONE: Implement way to respond directly to threaded client connection. Right now just output logs on exchange
DONE: Implement recovery
DONE: Implement leader election
TODO: Implement mutual funds?
DONE: heartbeats
 */
class ExchangeServer extends ReceiverAdapter {
    private static final int VOTE_TIMEOUT = 1000;
    private static final int HEARTBEAT_RATE = 200;
    private static final int LEADER = 0;
    private static final int FOLLOWER = 1;
    private static final int CANDIDATE = 2;
    // determines whether we're a leader, follower, or candidate
    private int state = FOLLOWER;
    private final Random rand = new Random();

    private JChannel channel;
    // helps initialize the cluster -- only initialize the loop once everyone has joined
    private int num_members;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    // The exchange is the state machine
    private Exchange exchange;
    private Timer timer = null;
    private int num_votes;
    private DB exchangeDB;

    // keep a list of clients with open transactions
    private ConcurrentHashMap<String, RunLock> clientList = new ConcurrentHashMap<>();

    /*
    I originally had the log transaction as a list when first testing it out, so that's why commitIndex
    is set to -1 and I always subtract 1 from lastApplied from using it
     */
    /*
        RAFT VARIABLES BELOW
         */
    private String serverAddr;

    // PERSISTENT ACROSS SERVERS
    // Last term a server has seen
    private Atomic.Integer currentTerm;
    // candidateID that received vote in current term
    private String votedFor = null;
    // Log of entries-- value is string of form term <transaction>
    private HTreeMap<Integer, String> log;
    private Atomic.Integer logidx;

    // VOLATILE ACROSS SERVERS
    // highest index of the log which has been committed
    private int commitIndex = -1;
    // index of highest log entry applied to state machine (db)
    private Atomic.Integer lastApplied;

    // VOLATILE STATE ON LEADERS
    // for each server, index of next log entry to send to server (initialized to
    // leader last log index + 1 (key = serverID, value = index)
    private final ConcurrentHashMap<String, Integer> nextIndex = new ConcurrentHashMap<>();
    // for each server, index of highest log entry known to be replicated
    // on server (initialized to 0)
    private final ConcurrentHashMap<String, Integer> matchIndex = new ConcurrentHashMap<>();

    // Invoked by leader to replicate log entries
    // Follower responds with currenterm + false/true
    private String appendEntries(AppendEntries ap) {
        if (ap.term > currentTerm.get()) {
            currentTerm.set(ap.term);
            convertToFollower();
        }

        String reply = null;
        int index = -1;
        int thisTerm;
        int entryTerm;

        // leaderCommit = leader's commit idx
        // reply "false" if our term is ahead of leader's
        if (ap.term < currentTerm.get()) {
            System.out.println("responding false: " + "apRespond " + currentTerm + " false");
            return "apRespond " + currentTerm + " false";
        }
        // Reply false if no entry at prevLogIndex whose term matches prevLogTerm
        try {
            if (ap.prevLogIndex == -1 || ((ap.prevLogIndex < log.size()) &&
                    (Integer.parseInt(log.get(ap.prevLogIndex).split(" ")[0])) == ap.prevLogTerm)) {
                reply = "true";
            } else {
                reply = "false";
                System.out.println("Responding false. prevlogidx: " + ap.prevLogIndex + "prevterm: " + ap.prevLogTerm +
                " my term: " + currentTerm + " my logsize: " + log.size() + " my logidx+ " + logidx);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("ap.prevlogidx: " + ap.prevLogIndex + "log size: " + log.size() + "logidx: " + logidx);
            System.exit(-1);
        }
        if(timer!=null) timer.cancel();
        int time = VOTE_TIMEOUT + rand.nextInt(VOTE_TIMEOUT);
        timer = new Timer();
        timer.schedule(new ElectionTimer(),  time);
        if (reply.equals("true")) {
            index = ap.prevLogIndex;

            // Scan through entries--if an existing entry conflicts with new one, delete that entry
            // and all following. Conflict = same index, diff terms
            if (!ap.entries.isEmpty()) {
                for (int i = 0; i < ap.entries.size(); i++) {

                    // entry exists in index
                    index++;
//                    if (log.size() == index) {
//                        String str = ap.entries.get(i);
//                        System.out.println(str);
//                        log.put(logidx.incrementAndGet(), ap.entries.get(i));
//                    }
//                    else {
                        // if a term on our list conflicts, wipe the current entry and all ahead
                        if(log.get(index)!=null) {
                            entryTerm = Integer.parseInt(ap.entries.get(i).split(" ")[0]);
                           thisTerm = Integer.parseInt(log.get(index).split(" ")[0]);
                            if (thisTerm != entryTerm) {
                                for(int j = index; j < log.size(); j++) {
                                    System.out.println("Removing entry " + j + "my log size " + log.size() + "my log idx " + logidx.get());
                                    log.remove(j);
                                    logidx.decrementAndGet();
                                }
                                log.put(logidx.getAndIncrement(), ap.entries.get(i));
                            }
                        }
                    else log.put(logidx.getAndIncrement(), ap.entries.get(i));
                }
            }
            commitIndex = Math.min(ap.leaderCommit, index);
        }

        // apply entries up to leadercommit to state machine
        while (commitIndex >= lastApplied.get()) {
            // either direct increment command (term increment) or a transaction (term propose client-id <transaction>)
            lastApplied.getAndIncrement();
            String transaction;
            String msg[] = log.get(lastApplied.get()-1).split(" ",4);
            if(msg.length == 2) transaction = msg[1];
            else transaction = msg[3];
            String exmsg =exchange.executeTransaction(transaction);
            if(exmsg!=null)System.out.println(exmsg);
            // it's your client
            if(msg.length==4 && clientList.containsKey(msg[2]) ) {
               // System.out.println("updating client in commit");
                update(msg[2], exmsg);
            }
        }
        return "apRespond " + currentTerm + " " + reply + " " + index;
    }

    // Invoked by candidates to gather votes
    // returns currentTerm + true/false vote
    private String requestVote(RequestVote rv) {
        System.out.println("Received vote request from someone with term " + rv.term + " my term is " + currentTerm);
        if (rv.term > currentTerm.get()) {
            currentTerm.set(rv.term);
            convertToFollower();
        }
        // if we haven't voted for a candidate and the candate's log is at least as up to date, vote yes
        if ((votedFor == null || votedFor.equals(rv.candidateID)) &&
                (commitIndex <= rv.lastLogIndex && rv.term >= currentTerm.get()))
            return "rvRespond " + currentTerm + " true";
        else return "rvRespond " + currentTerm + " false";
    }

    private void leaderAdvanceState() {
        List<Integer> indeces = new ArrayList<>(matchIndex.values());
        Collections.sort(indeces);
        int position = (channel.getView().getMembers().size() / 2);
        int n = indeces.get(position);
        if(n <= commitIndex) return;
        else {
            int thisTerm = Integer.parseInt(log.get(n).split(" ")[0]);
            if(thisTerm==currentTerm.get()) commitIndex = n;
            else return;
        }
        while (commitIndex >= lastApplied.get()) {
            // either direct increment command (term increment) or a transaction (term propose client-id <transaction>)
            lastApplied.getAndIncrement();
            String transaction;
            String[] msg;
            try {
                msg = log.get(lastApplied.get()-1).split(" ",4);
            }
            catch (NullPointerException e) {
                return;
            }
            // increment
            if(msg.length == 2) {
                transaction = msg[1];
                exchange.executeTransaction(transaction);
                //System.out.println(exchange.executeTransaction(transaction));
                return;
            }
            else transaction = msg[3];
            // send notifaction to the channel the client's transaction has been applied
            String notifymsg = "applied " + msg[2];
            String exmsg =exchange.executeTransaction(transaction);
            System.out.println(exmsg);
            // it's your client
            if(clientList.containsKey(msg[2]) && msg.length==4) {
                //System.out.println("updating leader client");
                update(msg[2], exmsg);
                return;
            }
        }

    }

    private void convertToFollower() {
        state = FOLLOWER;
        votedFor = null;
        int time = VOTE_TIMEOUT + rand.nextInt(VOTE_TIMEOUT);
        if(timer!=null) timer.cancel();
        timer = new Timer();
        timer.schedule(new ElectionTimer(), time);
    }

    // Increment term, vote for yourself, reset election timer
    private synchronized void convertToCandidate() {
        state = CANDIDATE;
        num_votes = 0;
        currentTerm.incrementAndGet();
        votedFor = serverAddr;
        System.out.println("Converting to candidate! requesting votes");
        if (timer != null) timer.cancel();
        timer = new Timer();
        try {
            channel.send(new Message(null, null, generateRV()));
        } catch (Exception e) {
            // will time out and try again
            e.printStackTrace();
        }
        int time = VOTE_TIMEOUT + rand.nextInt(VOTE_TIMEOUT);
        timer.schedule(new ElectionTimer(), time);

    }

    // parse a message sent by another member of the group
    private void parseMsg(String msg) {
        if (msg.equals("increment") && state == LEADER) log.put(logidx.getAndIncrement(), currentTerm + " " + msg);
    }

    private void leaderHandleTransaction(String transaction) {
        String client = transaction.split(" ")[1];
        // propose client <transaction>
        String retMsg = exchange.checkTransaction(transaction.split(" ",3)[2]);
        if (retMsg.equals("accepted")) {
            String logentry = currentTerm + " " + transaction;
            log.put(logidx.getAndIncrement(), logentry);
        }
        // rejected client <retMsg>
        else
        {
            System.out.println(client + " " +retMsg);
            // it's leader's client
            if( clientList.containsKey(client)){
                update(client, retMsg);
                return;
            }
            try {
                channel.send(new Message(null, null, "rejected " + client + " " + retMsg));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    // Set the nextindex of every follower to the max index of the leader + 1
    private void leaderInitialize() {
        System.out.println("This server has been elected the new leader on term: " + currentTerm);
        int maxindex = log.size();
        for (Address addr : channel.getView().getMembers()) {
            String name = channel.getName(addr);
            if (name.equals(serverAddr)) continue;
            nextIndex.put(name, maxindex);
            matchIndex.put(name, -1);
        }
        // Initialize periodic RPCs/heartbeats
        timer.cancel();
        timer = new Timer();
        timer.schedule(new AppendTask(), 0, HEARTBEAT_RATE);
    }

    public String fromClient(String msg, RunLock rl) {
        String[] msgarray = msg.split(" ",3);
        if (msgarray.length < 2)
            return "Invalid transaction";
        switch (msgarray[0]) {
            case "getSummary":
                if (msgarray.length != 2) return "Invalid transaction";
                if (!exchange.hasClient(msgarray[1])) return "You do not have an account";
                else return exchange.getSummary(msgarray[1]);
//            case "increment":
//                try {
//                    channel.send(new Message(null, null, "increment"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
            case "propose":
                // propose client-id transaction
                if (msgarray.length != 3) return "Invalid transaction";
                if (state == LEADER) {
                    clientList.put(msgarray[1], rl);
                    leaderHandleTransaction(msg);
                }
                else try {
                    channel.send(new Message(null, null, msg));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                clientList.put(msgarray[1], rl);
                break;
            default:
                return "Invalid transaction";
        }

        return null;
    }

    @Override
    public void receive(Message msg) {
        Address from = msg.getSrc();

        // ignore messages from ourself
        if (from.equals(channel.getAddress())) return;
        // handle requestvote
        Object msgObj = null;

        try {
            msgObj = Util.objectFromByteBuffer(msg.getBuffer());
        } catch (Exception e) {
            System.err.println("error getting object");
            e.printStackTrace();
        }
        if (msgObj instanceof RequestVote) {
            String vote = requestVote((RequestVote) msgObj);
            try {
                channel.send(new Message(from, null, vote));
            } catch (Exception e) {
                System.err.println("error getting vote request");
                e.printStackTrace();
            }
        } else if (msgObj instanceof AppendEntries) {
            if(state==LEADER) return;
            String response = appendEntries((AppendEntries) msgObj);
            try {
                channel.send(new Message(from, null, response));
            } catch (Exception e) {
                System.err.println("error getting append");
                e.printStackTrace();
            }
        }
        // It's a either a message passed from a client, result of a requestVote/appendEntries,
        // or a message from the server that your client's transaction has completed
        else {
            String[] msgline = ((String) msgObj).split(" ");
            if (msgline[0].equals("propose")) {
                if(state==LEADER) {

                    leaderHandleTransaction((String) msg.getObject());
                    return;
                }
                else return;

            }
            // accepted client <transaction>
            if (msgline[0].equals("accepted")) {
                if(clientList.containsKey(msgline[1])) {
                    msgline = ((String) msgObj).split(" ",3);
                    update(msgline[1], msgline[2]);
                    return;
                }
            }
            // rejected client <response>
            if (msgline[0].equals("rejected")) {
                if(clientList.containsKey(msgline[1])) {
                    msgline = ((String) msgObj).split(" ",3);
                    update(msgline[1], msgline[2]);

                }
                return;
            }
//            else if (msgline[0].equals("increment")) {
//                exchange.advanceHour();
//                return;
//            }
            // higher term = step down
            int term = Integer.parseInt(msgline[1]);
            if (term > currentTerm.get()) {
                currentTerm.set(term);
                convertToFollower();
            }
            if (state == CANDIDATE && currentTerm.get() == term && msgline[0].equals("rvRespond")) {
                if (msgline[2].equals("true")) {
                    num_votes++;
                    // you're the new leader
                    if (num_votes >= (channel.getView().size() / 2 + 1)) {
                        state = LEADER;
                        num_votes = 0;
                        leaderInitialize();
                    }
                }
            }
            if (state == LEADER && currentTerm.get() == term && msgline[0].equals("apRespond")) {
                // decrement and try again
                String name = channel.getName(from);
                if(msgline[2].equals("false")) {
                    System.out.println("negative response: " + msgObj + " my term: " + currentTerm.get() + " my log " +
                            "index+ " + logidx + " my log size: " +log.size());
                    nextIndex.put(name,Math.max(0,nextIndex.get(name)-1));
                    return;
                }
                int idx = Integer.parseInt(msgline[3]);
                // update the replicated index for the server we got a response from
                matchIndex.put(name, idx);
                nextIndex.put(name, idx + 1);
                leaderAdvanceState();
            }
        }
    }


    private RequestVote generateRV() {
        int llt;
        if (!log.isEmpty()) {
            llt = Integer.parseInt(log.get(logidx.get() -1).split(" ")[0]);
        } else llt = currentTerm.get();
        return new RequestVote(currentTerm.get(), serverAddr, logidx.get()-1, llt);
    }

    private void start(String id, String continent, int num_members) throws Exception {
        System.setProperty("java.net.preferIPv4Stack", "true");

        exchangeDB = DBMaker.fileDB(id + ".db").closeOnJvmShutdown().make();
        log = exchangeDB.hashMap("log", Serializer.INTEGER, Serializer.STRING).createOrOpen();
        logidx = exchangeDB.atomicInteger("logidx").createOrOpen();
        currentTerm = exchangeDB.atomicInteger("curTerm").createOrOpen();
        lastApplied = exchangeDB.atomicInteger("lastApplied").createOrOpen();

        if(logidx.equals(null)) {
            logidx.set(0);
            currentTerm.set(0);
            lastApplied.set(0);
        }

        channel = new JChannel();
        channel.setName(id);
        this.num_members = num_members;
        exchange = new Exchange(id, continent, exchangeDB);
        serverAddr = channel.getName();
        channel.setReceiver(this);
        channel.connect(continent);
        channel.getState(null, 10000);
        eventLoop();
    }

    private void eventLoop() {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // Start an election when the last member joins, but only on startup

        if(channel.getView().getMembers().size() == num_members && currentTerm.get()==0) convertToCandidate();
        while (true) {

            try {

                System.out.print("> ");
                System.out.flush();
                String line = in.readLine();

                if (line.startsWith("quit") || line.startsWith("exit"))
                    break;

                parseMsg(line);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        channel.close();
    }

    public void viewAccepted(View new_view) {
        System.out.println("Current members: " + new_view);
        System.out.println("# members: " + new_view.getMembers().size());
        // In case someone re-joins when we have been elected leader
        if(state==LEADER) {
            int maxindex = log.size();
            for (Address addr : channel.getView().getMembers()) {
                String name = channel.getName(addr);
                // Skip yourself or anyone who exists
                if (name.equals(serverAddr) || matchIndex.containsKey(name)) continue;
                nextIndex.put(name, maxindex);
                matchIndex.put(name, -1);
            }
        }
    }


    public static void main(String[] args) throws Exception {
        int port, num_members, udpport;
        String id;
        String continent;
        ExchangeServer ex = null;
        try {
            num_members = Integer.parseInt(args[3]);
            port = Integer.parseInt(args[2]);
            id = args[1];
            continent = args[0];
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            System.err.println("usage: java ExchangeServer <continent> <id> <tcp-port> <num-members>");
            return;
        }
        try {
            ex = new ExchangeServer();
            ServerTCP tcpserv = new ServerTCP(port, ex);
            new Thread(tcpserv).start();
            ex.start(id, continent, num_members);
        } catch (Exception e) {
            System.err.println("Start failed");
            e.printStackTrace();
        }
    }
    private class ElectionTimer extends TimerTask {
        @Override
        public void run() {
            // should only execute if there's a timeout--try new election
            System.out.println("Initializing election");
            convertToCandidate();
        }
    }

    private class AppendTask extends TimerTask {
        @Override
        public void run() {
//            System.out.println("Leader sending hb");
            doAppend();
        }
    }

    private synchronized void doAppend() {
        log.put(logidx.getAndIncrement(), currentTerm + " " + "increment");
        for (Address addr : channel.getView().getMembers()) {
            String name = channel.getName(addr);
            if(name.equals(serverAddr)) continue;
            ArrayList<String> entries;
            int previdx = nextIndex.get(name);
            int lastidx = log.size();
           // nextIndex.put(name,log.size());
            int prevterm;
            try {
                prevterm = Integer.parseInt(log.get(previdx-1).split(" ")[0]);
            } catch (NullPointerException e) {
                prevterm = currentTerm.get();
            }

            if (lastidx >= previdx) {
                entries = new ArrayList<>();
                for(int i = previdx; i < lastidx; i++) {
                    entries.add(log.get(i));
                }
            } else entries = new ArrayList<>();
            AppendEntries ap = new AppendEntries(currentTerm.get(), serverAddr, commitIndex, previdx - 1, prevterm, entries);
            try {
                channel.send(new Message(addr, null, ap));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void update(String client, String msg) {
            if(this.clientList.containsKey(client)) {
                RunLock rl = clientList.remove(client);
                rl.put(msg);
            }

        }


}