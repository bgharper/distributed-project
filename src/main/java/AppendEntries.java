import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by billobob on 5/24/16.
 */

// term = leader's term, leaderID = leader's ID, prevLogIndex = index of log entry preceding new ones,
// prevLogTerm = term of prevLogIndex entry, entries = log of entries to store (may be > 1),
@SuppressWarnings("ALL")
final class AppendEntries implements Serializable {
    final int term;
    final String leaderId;
    final int prevLogIndex;
    final int prevLogTerm;
    final ArrayList<String> entries;
    final int leaderCommit;
    // prevlogidx, prevlogterm, entries
    AppendEntries(int currentTerm, String serverAddr, int commitIndex,
                  int prevLogIndex, int prevLogTerm, ArrayList<String> entries) {
        this.prevLogIndex = prevLogIndex;
        this.term = currentTerm;
        this.leaderId = serverAddr;
        this.prevLogTerm = prevLogTerm;
        this.entries = entries;
        this.leaderCommit = commitIndex;
    }
}

