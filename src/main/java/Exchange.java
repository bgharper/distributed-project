/**
 * This is more or less a POJO representing the stock exchange
 * It holds three hashmaps--one for name + quantity + price,
 * another for Client name + stock name + quantity owned (
 * here an array of length 2--client id + stock name, is used as the
 * key), and the final for cash a client has
 * <p>
 * Includes methods for checking if a transaction is possible and executing transaction
 */

import org.mapdb.Atomic;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


class Exchange {


    private static final String PRICE_STOCKS_CSV = "price_stocks.csv";
    private static final String STOCKS_QTY_CSV = "qty_stocks.csv";

    // DB instance holding all the exchange data
    private final DB exchangeDB;


    // Key -- ticker symbol Item 0 -- quanity, item 1 -- price
    private final HTreeMap<String, double[]> exchangeMap;

    // Key client id+ticker symbol (comma delim), item -- shares owned by X client of Y stock
    private final HTreeMap<String, Double> ownerMap;

    // key -- client id, value -- cash
    private final HTreeMap<String, Double> bankMap;

    // Store the date string
    private final Atomic.String dateString;

    // Keep track of date
    private Calendar time;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    // CSV data -- hashmap with key as stock name, value as position in price/qty column
    // And string matrices holding data. Integer holds last row accessed
    // and how long the matrices are
    private final HashMap<String, Integer> stockIndex = new HashMap<>();
    private String[][] priceList;
    private String[][] qtyList;
    private final Atomic.Integer lastQtyRow;
    private final Atomic.Integer lastPriceRow;
    private int qtyRowNum;
    private int priceRowNum;


    public Exchange(String id, String continent, DB exchangeDB) {
        this.exchangeDB = exchangeDB;
        exchangeMap = exchangeDB.hashMap("exchangeMap", Serializer.STRING, Serializer.DOUBLE_ARRAY).createOrOpen();
        ownerMap = exchangeDB.hashMap("ownerMap", Serializer.STRING, Serializer.DOUBLE).createOrOpen();
        bankMap = exchangeDB.hashMap("bankmap", Serializer.STRING, Serializer.DOUBLE).createOrOpen();
        dateString = exchangeDB.atomicString("date").createOrOpen();
        lastPriceRow = exchangeDB.atomicInteger("lastPriceRow").createOrOpen();
        lastQtyRow = exchangeDB.atomicInteger("lastQtrRow").createOrOpen();
        if(lastPriceRow.equals(null)) {
            lastPriceRow.set(0);
            lastQtyRow.set(0);
        }

        try {
            String dateStr = (dateString.get()!=null) ? dateString.get() : "1/1/2016 7:00";
            System.out.println(dateStr);
            Date date = dateFormat.parse(dateStr);
            time = new GregorianCalendar();
            time.setTime(date);
            // read in the info for 8 am
            parseCSV(continent);
            //advanceHour();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    // Provide a summary to the client of stocks owned and cash flow
    public String getSummary(String client) {
        String sb = "Client: " + client + "\n" +
                getStockSummary(client) +
                "----Cash flow-----\n" +
                bankMap.get(client) + "\n" +
                "~";
        return sb;
    }

    // provide summary for stocks of individual client

    public String getStockSummary(String client) {
        StringBuilder sb = new StringBuilder();
        sb.append("----Stocks Owned-----\n");
        for (Object keys : ownerMap.keySet()) {
            String[] key = ((String) keys).split(" ");
            if (key[0].equals(client)) {
                sb.append(key[1]).append(": ").append(ownerMap.get(keys)).append(" shares\n");
            }
        }
        return sb.toString();
    }

    // Getter and setter methods for exchangeMap
    public double getStockQuanity(String ticker) throws NoStockException {
        try {
            return exchangeMap.get(ticker)[0];
        } catch (NullPointerException e) {
            throw new NoStockException("Stock " + ticker + " does not exist");
        }
    }

    public double getStockPrice(String ticker) throws NoStockException {
        try {
            return exchangeMap.get(ticker)[1];
        } catch (NullPointerException e) {
            throw new NoStockException("Stock  " + ticker + " ddoes not exist");
        }
    }

    private void setStockQuantity(String ticker, double quanity) {
        double[] arr = exchangeMap.get(ticker);
        // new stock
        if (arr == null) arr = new double[]{0.0, 0.0};
        arr[0] = quanity;
        exchangeMap.put(ticker, arr);
    }

    private void setStockPrice(String ticker, double price) {
        double[] arr = exchangeMap.get(ticker);
        arr[1] = price;
        exchangeMap.put(ticker, arr);
    }


    // getter and setter methods for ownerMap

    public double getClientOwned(String clientAndStock) throws NotOwnedException {
        try {
            return ownerMap.get(clientAndStock);
        } catch (NullPointerException e) {
            throw new NotOwnedException(e);
        }
    }

    private void setClientOwned(String clientAndStock, double quanity) {
        ownerMap.put(clientAndStock, quanity);
    }

    // getter and setter methods for bankMap

    public double getClientCash(String client) {
        // If a client has no cash, just initialize to 0
        try {
            return bankMap.get(client);
        } catch (NullPointerException e) {
            bankMap.put(client, 0.0);
            return bankMap.get(client);
        }
    }

    private void setClientCash(String client, double value) {
        bankMap.put(client, value);
    }

    public boolean hasClient(String client) {
        return bankMap.containsKey(client);
    }
    // Methods to see if it's possible for a client to buy/sell X shares
    // Returns "accept" on success, o/w returns an error message describing the failure

    public String checkTransaction(String line) {
        String transaction, client, ticker = null;
        String returnMsg = null;
        String[] args = line.split(",");

        // automatically accept an increment transaction
        if(args.length == 1) {
            if(args[0].equals("increment")) return "accepted";
            else return "error";
        }

        if ((args.length != 4) && (args.length != 3))
            return "error";

        transaction = args[0];
        client = args[1];
        Double quantity = null;
        // buy/sell
        try {
            if (args.length == 4) {
                ticker = args[2];
                quantity = Double.parseDouble(args[3]);
            } else {
                quantity = Double.parseDouble(args[2]);
            }
            try {
                double cashOnHand;
                double quantityStock;
                double stockPrice;
                double quanityOwned;
                switch (transaction) {
                    case "initialize":
                        if (bankMap.containsKey(client)) {
                            returnMsg = client + " already exists";
                        }
                        break;
                    case "buy":
                        cashOnHand = getClientCash(client);
                        quantityStock = getStockQuanity(ticker);
                        stockPrice = getStockPrice(ticker);
                        if (quantity > quantityStock) {
                            returnMsg = "Quanity requested " + quantity + " < inventory " + quantityStock;
                        } else if (cashOnHand < (quantity * stockPrice)) {
                            returnMsg = "Client " + client + " does not have enough cash";
                        }
                        break;
                    case "sell":
                        quanityOwned = getClientOwned(client + "," + ticker);
                        if (quantity > quanityOwned) {
                            returnMsg = "Client " + client + " quanity owned" + quanityOwned +
                                    " < requested sell amount " + quantity;
                        }
                        break;
                    default:
                        returnMsg = "error";
                }
            } catch (NoStockException e) {
                returnMsg = "Stock does not exist";
            } catch (NotOwnedException e) {
                returnMsg = "Client does not own stock";
            }
        } catch (Exception e) {
            returnMsg = "invalid transaction format";
        }

        if (returnMsg != null)
            return time.getTime().toString() + ": " + line + "rejected: " + returnMsg;
        else
            return "accepted";
    }

    // The next two method--executeTransaction--assumes that the check above was performed
    // in the proposal phase. returns "error" on... error, "executed" on executed

    public String executeTransaction(String line) {
        String transaction, client;
        //System.out.println(line);
        String[] args = line.split(",");
        // just increment and continue
        if(args[0].equals("increment")) {
            advanceHour();
            //return time.getTime().toString();
            return null;
        }

        if ((args.length != 4) && (args.length != 3)) {
            return "execute args error";
        }
        transaction = args[0];
        client = args[1];
        Double quantity, stockPrice, cost = 0.0, currentQuantity = 0.0, cashOnHand = 0.0, stockOnHand = 0.0;
        String clientAndStock = null, ticker = null;
        try {
            if (args.length == 4) {
                ticker = args[2];
                quantity = Double.parseDouble(args[3]);
                stockPrice = getStockPrice(ticker);
                cost = stockPrice * quantity;
                currentQuantity = getStockQuanity(ticker);
                cashOnHand = getClientCash(client);
                clientAndStock = client + "," + ticker;
                try {
                    stockOnHand = getClientOwned(clientAndStock);
                } catch (NotOwnedException e) {
                    stockOnHand = 0.0;
                }
            } else {
                quantity = Double.parseDouble(args[2]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "parse error";
        }


        switch (transaction) {
            case "initialize":
                setClientCash(client, quantity);
                break;
            case "buy":
                setClientOwned(clientAndStock, stockOnHand + quantity);
                setStockQuantity(ticker, currentQuantity - quantity);
                setClientCash(client, cashOnHand - cost);
                break;
            case "sell":
                setClientOwned(clientAndStock, stockOnHand - quantity);
                setStockQuantity(ticker, currentQuantity + quantity);
                setClientCash(client, cashOnHand + cost);
                break;
            default:
                return "transaction error";
        }

        if(args.length==4)
            return time.getTime().toString() + ": " + client +
                    " " + transaction + " " + ticker + " " + getStockQuanity(ticker);
        else return time.getTime().toString() + ": " + client +
                " " + transaction;
    }

    private void advanceHour() {
        time.add(Calendar.HOUR, 1);
        dateString.set(dateFormat.format(time.getTime()));
        String day;
        String hour;
        String dateStr;
        Date date;
        // We can't be sure the qty and price csvs are same length, so parse separately
        // Check both matrices if there's an
        try {
            int curRow = lastQtyRow.get();
            int TIME_POSITION = 1;
            int DATE_POSITION = 0;
            if (curRow < qtyRowNum) {
                day = qtyList[curRow][DATE_POSITION];
                hour = qtyList[curRow][TIME_POSITION];
                dateStr = day + " " + hour;
                date = dateFormat.parse(dateStr);
                if (time.getTimeInMillis() == date.getTime()) {
                    for (Map.Entry<String, Integer> entry : stockIndex.entrySet()) {
                        String key = entry.getKey();
                        int i = entry.getValue();
                        String qtyString = qtyList[curRow][i];
                        // do nothing for empty cells
                        if (!qtyString.equals("")) {
                            Double d = Double.parseDouble(qtyString);
                            Double cur;
                            // catch the exception if new -- assign 0.0 shares
                            try {
                                cur = getStockQuanity(key);
                            } catch (NoStockException e) {
                                cur = 0.0;
                            }
                            setStockQuantity(key, cur + d);
                        }
                    }
                    lastQtyRow.incrementAndGet();
                }
            }
            curRow = lastPriceRow.get();
            if (curRow < priceRowNum) {
                day = priceList[curRow][DATE_POSITION];
                hour = priceList[curRow][TIME_POSITION];
                dateStr = day + " " + hour;
                date = dateFormat.parse(dateStr);
                if (time.getTimeInMillis() == date.getTime()) {
                    for (Map.Entry<String, Integer> entry : stockIndex.entrySet()) {
                        String key = entry.getKey();
                        int i = entry.getValue();
                        String priceString = priceList[curRow][i];
                        // do nothing for empty cells
                        if (!priceString.equals("") && exchangeMap.containsKey(key)) {
                            Double d = Double.parseDouble(priceString);
                            setStockPrice(key, d);

//                            System.out.println("At time " + dateStr + " stock " + key + " has " + qty + " shares and "
//                                    + d + " price");
                        }

                    }
                    lastPriceRow.incrementAndGet();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void parseCSV(String continent) {
        LinkedList<String[]> priceRows = new LinkedList<String[]>();
        LinkedList<String[]> qtyRows = new LinkedList<String[]>();


        try (BufferedReader priceRead =
                     new BufferedReader(new FileReader(Exchange.PRICE_STOCKS_CSV));
             BufferedReader qtyRead =
                     new BufferedReader(new FileReader(Exchange.STOCKS_QTY_CSV))) {
            String row2;
            String row1;

            // First two lines special, gives us the stock locations in matrix
            row1 = priceRead.readLine();
            row2 = priceRead.readLine();
            String[] index = row2.split(",");
            String[] continents = row1.split(",");
            for (int i = 0; i < index.length; i++) {

                if (i >= 3 && continents[i].equals(continent)) {
                    stockIndex.put(index[i], i);

                }
            }

            // read rest of the rows in
            while ((row2 = priceRead.readLine()) != null)
                priceRows.addLast(row2.split(",", -1));

            // ignore first two lines, already pulled index
            qtyRead.readLine();
            qtyRead.readLine();

            while ((row2 = qtyRead.readLine()) != null) {
                qtyRows.addLast(row2.split(",", -1));
                String[] row = qtyRows.getLast();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        priceList = priceRows.toArray(new String[priceRows.size()][]);
        qtyList = qtyRows.toArray(new String[qtyRows.size()][]);
        priceRowNum = priceList.length;
        qtyRowNum = qtyList.length;

    }

    class NoStockException extends NullPointerException {
        public NoStockException() {
            super();
        }

        public NoStockException(String message) {
            super(message);
        }

        public NoStockException(String message, Throwable cause) {
            super(message);
        }
    }


    class NotOwnedException extends NullPointerException {
        public NotOwnedException() {
            super();
        }

        public NotOwnedException(String message) {
            super(message);
        }

        public NotOwnedException(Throwable cause) {
            super("Client does not own stock");
        }
    }
}