/**
 * Created by billobob on 5/16/16.
 * Thread to be spawned for each tcp connection to a member of exchange group
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class ServerRunnable implements Runnable{

    private Socket client = null;
    private boolean finished = false;
    private final ExchangeServer exchangeServer;
    String sendMsg;
    RunLock rl;
    public ServerRunnable(Socket clientSock, ExchangeServer ex, RunLock rl) {
        this.client = clientSock;
        this.exchangeServer = ex;
        this.rl = rl;
    }

    public void run() {
        try (
                PrintWriter out = new PrintWriter(client.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(client.getInputStream()))) {
            String OutputLine, InputLine;
            while(true) {
                if((InputLine = in.readLine()) == null || InputLine.equals("exit")) break;
                System.out.println(InputLine);
                String msgout = exchangeServer.fromClient(InputLine, rl);
                if(msgout!=null) out.println(msgout);
                else msgout = rl.get();

               out.println(msgout);
            }

        } catch (IOException e) { e.printStackTrace();}


        try {
            client.close();
        }
        catch (IOException e) { e.printStackTrace();}

    }

    public boolean getFinished() {
        return finished;
    }
    public RunLock getRl() { return rl;}

}