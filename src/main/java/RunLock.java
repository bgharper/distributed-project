/**
 * Created by billobob on 5/26/16.
 */
public class RunLock {
    private boolean empty = true;
    private String msg;

    public synchronized void put (String message) {
        empty = false;
        this.msg = message;
        notifyAll();
    }

    public synchronized String get() {
       // System.out.println("waiting for msg");
        while(empty) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        empty = true;
        notifyAll();
        return msg;
    }
}
