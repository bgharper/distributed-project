import csv
import datetime
import socket
import time
import random
import time
import sys
import threading
import time

stockdict = {'Europe': [], 'Asia': [], 'America': []}
with open('qty_stocks.csv', 'r') as testcsv:
    csvread = csv.reader(testcsv)
    i = 0
    rows = []
    for row in csvread:
        rows.append(row)
        i = i + 1
        if(i == 2):
            break
    for i in range(len(rows[0])):
        if(i < 3): continue
        stockdict[rows[0][i]] = stockdict[rows[0][i]] + [rows[1][i]]

portdict = {}
portdict['Asia'] = [5001, 5002, 5003, 5004, 5005, 5006, 5007]
portdict['America'] = [4001, 4002, 4003, 4004, 4005, 4006, 4007]
portdict['Europe'] = [3001, 3002, 3003, 3004, 3005, 3006, 3007]

transactions = ['buy', 'sell']
amounts = [1, 14, 23, 50, 200, 20000, 10]

class Client(threading.Thread):

    def __init__(self, num_transactions, clientid, continent,
                 stock=None, action=None, qty=None, port=None):
        threading.Thread.__init__(self)
        self.continent = continent
        self.clientid = clientid
        self.num_transactions = num_transactions
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.buf = ""
        self.stock = stock
        self.port = port
        self.action = action
        self.qty = qty


    def run(self):
        if(self.port is None):
            port = random.choice(portdict[self.continent])
        else:
            port = self.port
        self.sock.connect(('localhost', port))
        transaction = "propose " + str(self.clientid) + " " + ','.join(['initialize', str(self.clientid), str(200000)]) + '\n'
        print(transaction + '\n')
        self.sock.send(transaction.encode())
        for i in range(self.num_transactions):
            if(self.action is None):
                action = random.choice(transactions)
            elif(self.action == 'both'):
                if(i % 2 == 0):
                    action = 'buy'
                else:
                    action = 'sell'
            else:
                action = self.action
            if(self.qty is None):
                amount = str(random.choice(amounts))
            else:
                amount = self.qty
            if self.stock is None:
                stock = random.choice(stockdict[self.continent])
            else:
                stock = self.stock
            transaction = "propose " + str(self.clientid) + " " + ','.join([action, str(self.clientid), stock, str(amount)]) + '\n'
            print(transaction)
            try:
                self.sock.send(transaction.encode())
            except:
                self.sock.close()
                while True:
                    try:
                        self.sock.connect(('localhost', random.choice(portdict[self.continent])))
                        time.sleep(1)
                        print("Connection broken, trying to reconnect")
                        break
                    except:
                        pass
            time.sleep(1+int(random.random()*5))
            data = self.sock.recv(4096)
            string = data.decode('utf-8')
            print(string)
        # transaction = "getSummary" + " " + str(self.clientid) + '\n'
        # self.sock.send(transaction.encode())
        time.sleep(1)
        self.close()
        print(port)
    def close(self):
        self.sock.close()

    def listenToSockets(self):

        while True:

            changed_sockets = [self.sock]

            ready_to_read, ready_to_write, in_error = select.select(
                                                changed_sockets, [], [], 0.1)

            for s in ready_to_read:
                self.readDataFromSocket(s)

    # def readDataFromSocket(self, socket):

    #     data = ''
    #     self.buf = ''
    #     try:

    #         while True:
    #             data = socket.recv(4096)

    #             if not data: 
    #                 break

    #             self.buf += data
    #             if('\n' in self.buf):
    #                 print(self.buf.split('\n', 1)[0])
    #                 self.buf = self.buf.split('\n', 1)[1]

    #     except error, (errorCode,message): 
    #         # error 10035 is no data available, it is non-fatal
    #         if errorCode != 10035:
    #             print 'socket.error - ('+str(errorCode)+') ' + message

    #     if data:
    #         if buf != '':
    #             print(buf)
    #     else:
    #         print 'disconnected'  

if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print("Usage: python3 num_transactions num_clients [stock] [action] [qty] [port]")
        sys.exit(1)
    else:
        num_clients = int(sys.argv[2])
        num_transactions = int(sys.argv[1])
        stock = None
        qty = None
        action = None
        if(len(sys.argv) > 3):
            stock = sys.argv[3]
        else: stock = None
        if(len(sys.argv) > 4):
            action = sys.argv[4]
        if(len(sys.argv) > 5):
            qty = sys.argv[5]
        else: qty = None
        clients = []
        for i in range(num_clients):
            continent = random.choice(['Europe', 'America', 'Asia'])
            clients.append(Client(num_transactions, i+2, continent, stock, action, qty))
            clients[i].start()
        for i in range(num_clients):
            clients[i].join()