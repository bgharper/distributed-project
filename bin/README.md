JAR Usage:

java -jar exchange-all-0.1.jar <continent> <id> <tcp-port> <num-members>

Script usage:

python3 test_script_6.py <num_transactions> <num_clients> \[stock] \[action] \[quantity]

Where the last three must either all be used or not used at all. Only running
the first two arguments spins up a number of clients executing a number of transactions
for random amounts, stocks, and random continents. So e.g. it may request apple from asia,
which would fail.