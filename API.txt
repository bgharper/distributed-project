Notes to myself:

export CLASSPATH=./jgroups-3.6.9.Final.jar before running
javac ExchangeServer.java to compile
java ExchangeServer <tcp-port> to run each server

API:

CLIENT API:

propose <client id> <transaction-string> after connecting to an exchange

where transaction-string is of the format:
initialize,<client-id>,<amount-cash>
or
buy/sell,<client-id>,<stock>,<qty>

ExchangeServer commands (INTERNAL, SUBJECT TO CHANGE AS IMPLEMENTATION ADVANCES):
* these are implemented as jgroup msgs
  PROPOSE A TRANSACTION: propose <proposal-id> <transaction-string> 
  AGREE TO A TRANSACTION: agree <proposal-id> <transaction-string> 
    # AGREE msg is only sent to PROPOSER
  ACCEPT REQUEST TO COMMIT A TRANSACTION: accept request <proposal-id> <transaction-string>
    # ISSUED by PROPOSER

  ---- 
  should increment be part of the proposal chain? or just assumed?

  -----
  ExchangeServer terminal-based commands:
  prpose (same as above)
  incement (advance hour of exchange)
  

Exchange commands:

checkTransaction("buy/sell,client,stock,quantity")
  RETURNS
    errorMsg OR "accept"
executeTransacation("buy/sell,client,stock,quantity")
  RETURNS
    "error" OR "executed"

